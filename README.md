## Source code of Qualcomm SOCs msm8610, msm8625, msm8909, msm8916, msm8926, msm8939 and msm8974.

### AMSS source code distibutions (modem/baseband, wifi, bootloaders, trustzone, rpm
- msm8916_2014-12-03_amss_qrd: https://gitlab.com/qcom-sources15/msm8916_2014-12-03_amss_qrd/
- msm8974_2013-06-28_amss_oem: https://gitlab.com/qcom-sources15/msm8974_2013-06-28_amss_oem/
- msm8974_2013-10-29_amss_oem: https://gitlab.com/qcom-sources15/msm8974_2013-10-29_amss_oem/
- msm8909_2015-09-01_amss_device: https://gitlab.com/qcom-sources15/msm8909_2015-09-01_amss_device/
- msm8939_2014-11-06_amss_device: https://gitlab.com/qcom-sources15/msm8939_2014-11-06_amss_device/
- msm8939_2014-12-08_amss_device: https://gitlab.com/qcom-sources15/msm8939_2014-12-08_amss_device/
- msm8939_2014-12-08_amss_oem: https://gitlab.com/qcom-sources15/msm8939_2014-12-08_amss_oem/

#### Android source code including source code of qualcomm proprietary modules
- msm8610_2014-07-24: https://gitlab.com/qcom-sources15/msm8610_2014-07-24/
- msm8625_2013-03-26: https://gitlab.com/qcom-sources15/msm8625_2013-03-26/
- msm8926_2014-05-16: https://gitlab.com/qcom-sources15/msm8926_2014-05-16/
- msm8926_2014-07-19: https://gitlab.com/qcom-sources15/msm8926_2014-07-19/

### Version info
|msm8625||
|-|-
|Build date|2013-03-26 
|Repo|QRD8625LA20
|Android|4.1.2

|msm8974||
|-|-
|Build date|2013-06-28
|Repo|AU_LINUX_ANDROID_JB_MR1_RB1.04.02.02.050.116_msm8974_JB_MR1_RB1_CL3904528_release_AU
|Parent|jb_mr1_rb1.69 (2013-01-28)
||
|Build date|2013-10-29
|Repo|AU_LINUX_ANDROID_JB_MR1_RB1.04.02.02.050.175_msm8974_JB_MR1_RB1_CL3847824_release_AU
|Parent|jb_mr1_rb1.69 (2013-10-29)

|msm8610||
|-|-
|Build date|2014-07-24
|Repo|AU_LINUX_ANDROID_LNX.LA.3.5.3.4.04.04.02.113.008_msm8610_LNX.LA.3.5.3.4__release_AU
|Parent|LNX.LA.3.5.3 (2014-07-24)

|msm8926||
|-|-
|Build date|2014-05-16
|Repo|AU_LINUX_ANDROID_LNX.LA.3.5.3.1.04.04.02.106.003
|Parent|LNX.LA.3.5.3 (2014-05-16)
||
|Build date|2014-07-19
|Repo|AU_LINUX_ANDROID_LNX.LA.3.5.3.1.04.04.02.106.021
|Parent|LNX.LA.3.5.3 (2014-08-02)

|msm8909||
|-|-
|Build date|2015-09-01
|Repo|AU_LINUX_ANDROID_LA.BR.1.2.3_RB1.05.01.01.036.099
|Parent|LA.BR.1.2.3_rb1.76 (2015-09-01)

|msm8916||
|-|-
|Build date|2014-12-03
|Repo|AU_LINUX_ANDROID_LNX.LA.3.7.2.1_RB1.04.04.04.157.037
|Parent|???

|msm8939||
|-|-
|Build date|2014-11-06
|Repo|AU_LINUX_ANDROID_LA.BR64.1.1_RB1.05.00.00.001.043
|Parent|LA.BR64.1.1_RB1.10 (2014-10-06)
||
|Build date|2014-12-08
|Repo|AU_LINUX_ANDROID_LA.BR.1.1.2_RB1.05.00.00.031.006
|Parent|LA.BR.1.1.2-00610-8x16.0-1 (2014-12-08)

